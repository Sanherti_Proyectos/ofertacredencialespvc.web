<?php
  require_once 'vendor/autoload.php';

  function cleanupentries($entry)
  {
  	$entry = trim($entry);
  	$entry = stripslashes($entry);
  	$entry = htmlspecialchars($entry);

  	return $entry;
  }

  if (empty($_POST['countdownTimer']) || empty($_POST['name']) || empty($_POST['email']))
  {
    echo "Datos vacios";//('<pre>'); var_dump($_POST); echo('</pre>');
    exit;
  }

  $formData = [
    'countdownTimer' => cleanupentries($_POST['countdownTimer']),
    'recommenderEmail' => cleanupentries($_POST['recommenderEmail']),
    'name' => cleanupentries($_POST['name']),
    'email' => cleanupentries($_POST['email']),
    'tel' => cleanupentries($_POST['tel']),
    'comActivity' => cleanupentries($_POST['comActivity']),
    'cantidad' => cleanupentries($_POST['cantidad']),
    'city' => cleanupentries($_POST['city']),
    'comment' => cleanupentries($_POST['comment']),
    'browser' =>  cleanupentries($_POST['browser'])
  ];

  $total = $formData['cantidad'] * 14.9 + 174.00;

  $mail = new PHPMailer;
  $mail->CharSet = "UTF-8";

  $mail->setFrom($formData['email'], $formData['name']);
  $mail->addAddress('ventas@sanherti.com', 'Ventas Sanher Ti');

  $mail->Subject = 'Han enviado datos desde credencialesenpvc.sanherti.com';
  $mail->Body    = '<h2>Datos del formulario:</h2> <br />'
  . '<b>Contador:</b> ' . $formData['countdownTimer'] . '<br /><br />'
  . '<b>E-mail recomendador: </b>' . $formData['recommenderEmail'] . '<br />'
  . '<b>Nombre: </b>' . $formData['name'] . '<br />'
  . '<b>E-mail: </b>' . $formData['email'] . '<br />'
  . '<b>Teléfono: </b>' . $formData['tel'] . '<br />'
  . '<b>Actividad comercial: </b>' . $formData['comActivity'] . '<br />'
  . '<b>Cantidad pvc: </b>' . $formData['cantidad'] . '<br />'
  . '<b>Ciudad: </b>' . $formData['city'] . '<br />'
  . '<b>Comentario: </b>' . $formData['comment'] . '<br />'
  . '<b>Navegador: </b>' . $formData['browser'] . '<br />';

  $mail->AltBody    = 'Datos del formulario. '
  . 'Contador: ' . $formData['countdownTimer']
  . 'E-mail recomendador: ' . $formData['recommenderEmail']
  . 'Nombre: ' . $formData['name']
  . 'E-mail: ' . $formData['email']
  . 'Teléfono: ' . $formData['tel']
  . 'Actividad comercial: ' . $formData['comActivity']
  . 'Cantidad pvc: ' . $formData['cantidad']
  . 'Ciudad: ' . $formData['city']
  . 'Comentario: ' . $formData['comment']
  . 'Navegador: ' . $formData['browser'];


  if( !$mail->send() ) {
    $response = [
      'sended' => false,
      'error' => $mail->ErrorInfo
    ];
    echo json_encode($response);
  } else {
    // Envíarles un correo como respuesta
    $mailResponse = new PHPMailer;
    $mailResponse->CharSet = "UTF-8";

    $mailResponse->setFrom('contacto@sanherti.com', 'Contacto Sanher Ti');
    $mailResponse->addAddress($formData['email'], $formData['name']);
    $mailResponse->Subject = 'Credenciales en pvc';
    $mailResponse->Body =  '<p align="center"> Hola '. $formData['name'] .', recibimos tu petición para
                            el servicio de credenciales en pvc.
                            <br />
                            <h2 align="center">Éste es el resumen de tu cotización</h2>
                            <br />'
                            .'<p align="center"><b>Cantidad de credenciales: </b>' . $formData['cantidad'] . '<br /></p>'
                            .'<p align="center"><b>Precio: </b> $14.90 <br /></p>'
                            .'<p align="center"><b>Diseño plantilla: </b> $174.00 <br /></p>'
                            .'<p align="center"><b>Total neto: </b> <font color="#5cb85c"> $' . $total . '</font><br /></p>'
                            .'<br />
                            <p align="center">Agradecemos tú interes en nuestro servicio</p>
                            <br />
                            <p align="center">**La oferta sólo es valida 15 días a partir de ahora.</p>
                            <br />
                            <p align="center">Somos una empresa que ofrece servicios de integración a partir de un
                            modelo llamado digitalización de empresas.</p>
                            <br />
                            <p align="center">También ofrecemos:
                            <br /><br />
                            - Paginas Web.
                            <br />
                            - Plataformas de promociones para clientes.
                            <br />
                            - Aplicaciones móviles.
                            <br />
                            - Marketing Social.
                            <br /><br />
                            y cualquier trabajo a medida que permita ahorrarte tiempo y dinero
                            en procesos de tu empresa o negocio.</p>
                            <br />
                            <p align="center">Si quieres saber más entra en:
                            <br /><br />
                            <a href="www.sanherti.com">Sanher Ti</a>
                            <br /><br />
                            o contáctanos al número:
                            <br />
                            Oficina: 1525 1907.
                            <br />
                            whatsapp: (33) 1210 0172.
                            <br /><br />
                            Esperamos puedas disfrutar de nuestra promoción así como de nuestra
                            calidad y valor agregado en nuestros servicios</p>';

    $mailResponse->AltBody ='<p align="center"> Hola '. $formData['name'] .', recibimos tu petición para
                            el servicio de credenciales en pvc.
                            <br />
                            <h2 align="center">Éste es el resumen de tu cotización</h2>
                            <br />'
                            .'<p align="center"><b>Cantidad de credenciales: </b>' . $formData['cantidad'] . '<br /></p>'
                            .'<p align="center"><b>Precio: </b> $14.90 <br /></p>'
                            .'<p align="center"><b>Diseño plantilla: </b> $174.00 <br /></p>'
                            .'<p align="center"><b>Total neto: </b> <font color="#5cb85c"> $' . $total . '</font><br /></p>'
                            .'<br />
                            <p align="center">Agradecemos tú interes en nuestro servicio</p>
                            <br />
                            <p align="center">**La oferta sólo es valida 15 días a partir de ahora.</p>
                            <br />
                            <p align="center">Somos una empresa que ofrece servicios de integración a partir de un
                            modelo llamado digitalización de empresas.</p>
                            <br />
                            <p align="center">También ofrecemos:
                            <br /><br />
                            - Paginas Web.
                            <br />
                            - Plataformas de promociones para clientes.
                            <br />
                            - Aplicaciones móviles.
                            <br />
                            - Marketing Social.
                            <br /><br />
                            y cualquier trabajo a medida que permita ahorrarte tiempo y dinero
                            en procesos de tu empresa o negocio.</p>
                            <br />
                            <p align="center">Si quieres saber más entra en:
                            <br /><br />
                            http://www.sanherti.com/
                            <br /><br />
                            o contáctanos al número:
                            <br />
                            Oficina: 1525 1907.
                            <br />
                            whatsapp: (33) 1210 0172.
                            <br /><br />
                            Esperamos puedas disfrutar de nuestra promoción así como de nuestra
                            calidad y valor agregado en nuestros servicios</p>';

    $mailResponse->send();

    $response = [
      'sended' => true,
    ];
    echo json_encode($response);
  }
?>
