// IIFE - Immediately Invoked Function Expression
(function(yourcode)
{
    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);

}
(function($, window, document)
{
    // The $ is now locally scoped
    $(function()
    {
      var BrowserDetect =
      {
              init: function ()
              {
                  this.browser = this.searchString(this.dataBrowser) || "Other";
                  this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
              },
              searchString: function (data)
              {
                  for (var i = 0; i < data.length; i++)
                  {
                      var dataString = data[i].string;
                      this.versionSearchString = data[i].subString;

                      if (dataString.indexOf(data[i].subString) !== -1)
                      {
                          return data[i].identity;
                      }
                  }
              },
              searchVersion: function (dataString)
              {
                  var index = dataString.indexOf(this.versionSearchString);
                  if (index === -1)
                  {
                      return;
                  }

                  var rv = dataString.indexOf("rv:");
                  if (this.versionSearchString === "Trident" && rv !== -1)
                  {
                      return parseFloat(dataString.substring(rv + 3));
                  }
                  else
                  {
                      return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
                  }
              },
              dataBrowser:
              [
                  {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
                  {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
                  {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
                  {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
                  {string: navigator.userAgent, subString: "Opera", identity: "Opera"},
                  {string: navigator.userAgent, subString: "OPR", identity: "Opera"},

                  {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
                  {string: navigator.userAgent, subString: "Safari", identity: "Safari"}
              ]
        };
        BrowserDetect.init();

        function sendFormData( event )
        {
            // stop form from submitting normally
            event.preventDefault();

            var $form = $( this );
            // get the countdown timer's value
            var countdownValue = $( '#countdown-timer' ).text();
            // get the email reference
            var recommenderEmail = $( '#recommender-email' ).val();
            // get values from the form and concatenate the countdown value
            var browser = BrowserDetect.browser + ' ' + BrowserDetect.version;
            var formData = $form.serialize();
            formData = formData.concat('&countdownTimer='+countdownValue);
            formData = formData.concat('&recommenderEmail='+recommenderEmail);
            formData = formData.concat('&browser='+browser);

            console.log(formData);

            // post the values and return a message if the data were sent correctly
            var posting = $.post('mailer.php', formData,
            function( data )
            {
                if(data.sended === true)
                {
                    window.location.replace("thanks.html");
                    // $.growl.notice({
                    //     title: 'Datos enviados',
                    //     message: '¡Gracias! Nos pondremos en contacto contigo lo más pronto posible'
                    // });
                }
            }, 'json');

            posting.fail(function( data )
            {
                console.log(data);
                $.growl.error({ message: 'Lo sentimos, ha ocurrido un error al enviar los datos :(' });
            })
        }

        var offerForm = $('#offer-form');
        offerForm.on('submit', sendFormData);

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
        {
            $("#offer-form")
                .appendTo("#form-mobile");
        }
    });
}
));
