var secondsRemaining;
var intervalHandle;
var timeDisplay = document.getElementById('countdown-timer');

function tick() {

    //turn seconds into hh:mm:ss
    var hr  = Math.floor((secondsRemaining / 60) / 60);
    var min = ( Math.floor(secondsRemaining / 60) ) - (hr * 60);
    var sec = secondsRemaining - (Math.floor(secondsRemaining / 60) * 60);

    //add leading 0 if minutes less than 10
    if (hr < 10) {
        hr = '0' + hr;
    }
    //add leading 0 if minutes less than 10
    if (min < 10) {
        min = '0' + min;
    }
    //add leading 0 if seconds less than 10
    if (sec < 10) {
        sec = '0' + sec;
    }
    //concatenate with colon
    var message = hr + ':' + min.toString() + ':' + sec ;
    // now change the display
    timeDisplay.innerHTML = message;

    //stop if down to zero
    if (secondsRemaining === 0) {
        clearInterval(intervalHandle);
    }
    // subtract from seconds remaining
    secondsRemaining--;

}

function getsecondsRemaining() {

    var currentTime = Date.parse(new Date());
    var deadline = parseInt(localStorage.getItem('deadline'));

    return (deadline - currentTime) / 1000;
}

function startCountdown() {
    //call tick
    intervalHandle = setInterval(tick, 1000);
}

window.onload = function () {
    // set deadline if not exists
    if (localStorage.getItem('deadline') === null) {
        var timeInMinutes = 120;
        var currentTime = Date.parse(new Date());
        var deadline = Date.parse(new Date(currentTime + timeInMinutes*60*1000));
        localStorage.setItem('deadline', deadline);
        secondsRemaining = getsecondsRemaining();
        startCountdown();
    } else {
        secondsRemaining = getsecondsRemaining();
        if (secondsRemaining > 1)
            startCountdown();
    }

}
